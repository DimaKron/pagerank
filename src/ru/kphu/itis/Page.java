package ru.kphu.itis;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Page {

	private String pageUrl;
	
	private List<Page> avaiblePages;
	
	public Page(String pageUrl) {
		this.pageUrl = pageUrl;
		this.avaiblePages = new ArrayList<Page>();
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}

	public List<Page> getAvaiblePages() {
		return avaiblePages;
	}

	public void setAvaiblePages(List<Page> avaiblePages) {
		this.avaiblePages = avaiblePages;
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((Page)obj).pageUrl.equals(pageUrl);
	}
	
	@Override
	public String toString() {
		String avaiblePagesStr = "";
		for(Page page: avaiblePages){
			avaiblePagesStr += "\n   " + page.getPageUrl() ;
		}
		return pageUrl + avaiblePagesStr;
	}
	
	public boolean containsInAvaiblePages(Page page){
		for(Page avaiblePage: avaiblePages){
			if(avaiblePage.equals(page)){
				return true;
			}
		}
		return false;
	}
	
}
