package ru.kphu.itis;

import java.util.List;

public class PageRank {
	
	private static final double COEF = 0.85;
	
	private double pageRank = -1;
	
	public double computePageRank(List<List<Integer>> matrix){
	
		int n = matrix.size();
		
		int[] sumLink = new int[n];
		
		double sum;
		double eps;
		
		double[] pagesRankOld = new double[n];
		double[] pagesRank = new double[n];
		
		for (int i = 0; i < n; i++){
			pagesRankOld[i] = 1;
		}
		
		for(int j=0; j<n; j++){
			int sumL = 0;
			for(Integer i: matrix.get(j)){
				sumL += i;
			}
			sumLink[j] = sumL;
		}
		
		do{
			eps = 0;
			pageRank = 0;
			
			for (int j = 0; j < n; j++){
				sum = 0;
				pagesRank[j] = (1 - COEF);
				
				for (int i = 0; i < n; i++){
					if (matrix.get(j).get(i) > 0){
						sum += pagesRankOld[i] / sumLink[i];
					} else {
						sum += 0;
					}
				}
				
				sum = COEF * sum;
				pagesRank[j] += sum;
			}
			
			for (int i = 0; i < pagesRank.length; i++){
				pageRank += pagesRank[i];
			}
			
			for (int i = 0; i < pagesRank.length; i++){
				eps += (pagesRank[i] - pagesRankOld[i]) * (pagesRank[i] - pagesRankOld[i]);
			}
			
			pagesRankOld = pagesRank;
		} while (eps > 0.0001);
		
		return pageRank;
	}
	
	public void printPageRank(){
		System.out.println(pageRank);
	}
	
	
}
