package ru.kphu.itis;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Pages {
	
	private List<Page> allPages;
	
	public Pages(){
		allPages = new ArrayList<Page>();
	}
	
	public int size(){
		return allPages.size();
	}
	
	public void add(Page newPage){
		for(Page page: allPages){
			if(page.equals(newPage)){
				return;
			}
		}
		allPages.add(newPage);
	} 
	
	public void printPages(){
		for(int i=0; i<allPages.size(); i++){
			System.out.println(i+" "+ allPages.get(i));
		}
	}
	
	public List<Page> getPages(){
		return allPages;
	}
	
	public List<List<Integer>> getAvaibilityMatrix(){
		List<List<Integer>> matrix = new ArrayList<List<Integer>>();
		for(Page page1: allPages){
			List<Integer> line = new ArrayList<Integer>();
			for(Page page2: allPages){
				if(page1.equals(page2)){
					line.add(0);
				} else {
					line.add(page1.containsInAvaiblePages(page2)?1:0);
				}
			}
			matrix.add(line);
		}
		return matrix;
	}
	
	public void printAvaibilityMatrix(){
		for(List<Integer> line: getAvaibilityMatrix()){
			System.out.println(Arrays.toString(line.toArray()));
		}
	}
	
}
