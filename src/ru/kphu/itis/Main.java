package ru.kphu.itis;

public class Main {

	public static void main(String[] args) {
		
		// TASK 1
		Parser parser = new Parser();
	    parser.compute();
	    //parser.printPages();
	    //parser.printAvaibilityMatrix();
	    
	    // TASK 2: with bugs
	    PageRank pageRank = new PageRank();
	    pageRank.computePageRank(parser.getResult());
	    pageRank.printPageRank();
	}
}
