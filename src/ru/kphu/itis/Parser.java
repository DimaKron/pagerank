package ru.kphu.itis;
import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;


public class Parser {

	private static final String ROOT_PAGE_URL = "http://kpfu.ru/"; 
	private static final String MAIN_PART = "kpfu.ru"; 
	private static final String HTTP_PART = "http://";
	private static final String HTTPS_PART = "https://";
	
	private static final int PAGES_COUNT = 100; 
	
	
	private Pages pages;

	public Parser() {
	    pages = new Pages();
	}
	
	public void compute(){
		pages.add(new Page(ROOT_PAGE_URL));
		extractMainLinks(ROOT_PAGE_URL);
		findLinksForPages(pages.getPages());
	}

	private void extractMainLinks(String link){
		try {
			Document document = Jsoup.connect(link).get();
			Elements childLinks = document.select("a[href]");
			for(int i=0; i<childLinks.size(); i++){
				String childLink = childLinks.get(i).attr("href");
				if(childLink.contains(MAIN_PART) && (childLink.startsWith(HTTP_PART) || childLink.startsWith(HTTPS_PART))){
					if(pages.size()<PAGES_COUNT){
						pages.add(new Page(childLink));
					} else {
						break;
					}
				}
			}
			for(int i=0; i<childLinks.size(); i++){
				String childLink = childLinks.get(i).attr("href");
				if(childLink.contains(MAIN_PART) 
						&& (childLink.startsWith(HTTP_PART) || childLink.startsWith(HTTPS_PART)) && pages.size()<PAGES_COUNT){
					extractMainLinks(childLink);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void findLinksForPages(List<Page> pages) {
		for(Page page: pages){
			try {
				Document document = Jsoup.connect(page.getPageUrl()).get();
				Elements childLinks = document.select("a[href]");
				for(int i=0; i<childLinks.size(); i++){
					String childLink = childLinks.get(i).attr("href");
					if(childLink.contains(MAIN_PART) && (childLink.startsWith(HTTP_PART) || childLink.startsWith(HTTPS_PART))){
						page.getAvaiblePages().add(new Page(childLink));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void printPages(){
		pages.printPages();
	}
	
	public void printAvaibilityMatrix(){
		pages.printAvaibilityMatrix();
	}
	
	public List<List<Integer>> getResult(){
		return pages.getAvaibilityMatrix();
	}

}
